package spry.net.deckoilcalculator;

import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class CalcActivity extends Activity
{

	private Double _longitude = 0.0;
	private Double _latitude = 0.0;

	public Double getLongitude()
	{
		return _longitude;
	}

	public void setLongitude(Double _longitude)
	{
		this._longitude = _longitude;
	}

	public Double getLatitude()
	{
		return _latitude;
	}

	public void setLatitude(Double _latitude)
	{
		this._latitude = _latitude;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_calc);
		getWindow().setBackgroundDrawableResource(R.drawable.background_image);

		// StrictMode.ThreadPolicy policy = new
		// StrictMode.ThreadPolicy.Builder().permitAll().build();

		// StrictMode.setThreadPolicy(policy);

		// Define a listener that responds to location updates
		LocationListener locationListener = new LocationListener()
		{
			public void onLocationChanged(Location location)
			{
				// Called when a new location is found by the network location
				// provider.
				// makeUseOfNewLocation(location);
			}

			public void onStatusChanged(String provider, int status,
					Bundle extras)
			{
			}

			public void onProviderEnabled(String provider)
			{
			}

			public void onProviderDisabled(String provider)
			{
			}
		};


		PreferenceManager.setDefaultValues(this, R.xml.deck_oil_prefs, false);

		SharedPreferences sharedPrefs = PreferenceManager
				.getDefaultSharedPreferences(this);
		if (sharedPrefs.getBoolean("pref_key_get_weather", false))
		{
            LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            // Register the listener with the Location Manager to receive location
            // updates
            locationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
            // LocationProvider locationProvider = LocationManager.NETWORK_PROVIDER;
            // Or use LocationManager.GPS_PROVIDER

            Location lastKnownLocation = locationManager
                    .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

            if(lastKnownLocation != null)
            {
                _latitude = lastKnownLocation.getLatitude();
                _longitude = lastKnownLocation.getLongitude();
            }

			LongRunningGetIO lr = new LongRunningGetIO();
			lr.setActivity(this);
			lr.execute();
		}

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		// Handle item selection
		switch (item.getItemId())
		{
		case R.id.menu_settings:
			Intent settingsIntent = new Intent(this, SettingsActivity.class);
			startActivity(settingsIntent);
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_calc, menu);
		return true;
	}

	Double getControlValue(int resId) throws Exception
	{
		return Double.valueOf(((EditText) findViewById(resId)).getText()
				.toString());
	}

	public void calculate(View view)
	{
		try
		{
			Double lsq = getControlValue(R.id.lmsq);
			Double lenght = getControlValue(R.id.length);
			Double width = getControlValue(R.id.width);
			Double coats = getControlValue(R.id.coats);

			Double ammount = ((lenght * width) / lsq) * coats;
			DecimalFormat newFormat = new DecimalFormat("#.##");
			TextView textView = (TextView) findViewById(R.id.amountOil);
			textView.setText(newFormat.format(ammount) + "l");

		}
		catch (Exception e)
		{
			showAlert();
		}
	}

	private void showAlert()
	{
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

		// set title
		alertDialogBuilder.setTitle("Error!");

		// set dialog message
		alertDialogBuilder
				.setMessage(
						"One or more of the fields are empty or Liters per sqm has a 0 in it!")
				.setPositiveButton("OK", new DialogInterface.OnClickListener()
				{
					public void onClick(DialogInterface dialog, int id)
					{
						// do things
					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}

	private class LongRunningGetIO extends AsyncTask<Void, Void, String>
	{
		private CalcActivity _activity = null;

		public void setActivity(CalcActivity _activity)
		{
			this._activity = _activity;
		}

		protected String getASCIIContentFromEntity(HttpEntity entity)
				throws IllegalStateException, IOException
		{
			InputStream in = entity.getContent();
			StringBuffer out = new StringBuffer();
			int n = 1;
			while (n > 0)
			{
				byte[] b = new byte[4096];
				n = in.read(b);
				if (n > 0)
					out.append(new String(b, 0, n));
			}
			return out.toString();
		}

		@Override
		protected String doInBackground(Void... params)
		{
			String url = "http://api.wunderground.com/api/6fa5fc4305a3815e/geolookup/conditions/forecast/q/";// -37.777,144.91.json";
			url += _activity.getLatitude().toString() + ","
					+ _activity.getLongitude().toString();
			url += ".json";
			HttpClient httpClient = new DefaultHttpClient();
			HttpContext localContext = new BasicHttpContext();
			HttpGet httpGet = new HttpGet(url);
			String text;
			try
			{
				HttpResponse response = httpClient.execute(httpGet,
						localContext);
				HttpEntity entity = response.getEntity();
				text = getASCIIContentFromEntity(entity);
			}
			catch (Exception e)
			{
				return e.getLocalizedMessage();
			}
			return text;
		}

		protected void onPostExecute(String results)
		{

			try
			{
				if (results != null)
				{
					JSONObject jObject = new JSONObject(results);
					JSONObject jObject2 = jObject
							.getJSONObject("current_observation");
					String temp = jObject2.getString("temp_c");
					String conditions = jObject2.getString("weather");
					TextView textViewTemp = (TextView) findViewById(R.id.temperature);
					textViewTemp.setText(temp + "C");

					TextView textViewConditions = (TextView) findViewById(R.id.conditions);
					textViewConditions.setText(conditions);

					JSONObject forecast = jObject.getJSONObject("forecast");
					JSONObject txt_forecast = forecast
							.getJSONObject("txt_forecast");
					JSONArray array = txt_forecast.getJSONArray("forecastday");
					String fCast = new JSONObject(array.get(0).toString())
							.getString("fcttext_metric");
					TextView fcast = (TextView) findViewById(R.id.forecast);
					fcast.setText(fCast);
				}
			}
			catch (JSONException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			/*
			 * Button b = (Button) findViewById(R.id.my_button);
			 * b.setClickable(true);
			 */
		}
	}

}
